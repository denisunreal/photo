<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");
?>
<?php $APPLICATION->ShowPanel(); ?>
<? $APPLICATION->IncludeComponent(
    "bitrix:main.register",
    "registration",
    [
        "AUTH" => "Y",
        "REQUIRED_FIELDS" => ["PERSONAL_PHONE"],
        "SET_TITLE" => "Y",
        "USE_CAPTCHA" => "Y",
        "SHOW_ERRORS" => "Y",
        "SHOW_FIELDS" => ["PERSONAL_PHONE"],
        "SUCCESS_PAGE" => "/albums/",
        "USER_PROPERTY" => [],
        "USER_PROPERTY_NAME" => "",
        "USE_BACKURL" => "Y",
    ]
); ?><?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>