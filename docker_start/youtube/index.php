<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("YouTube");
?><? $APPLICATION->IncludeComponent(
    "project:youtube.complex",
    "",
    array(
        "SEF_FOLDER" => "/youtube/",
        "SEF_MODE" => "Y",
        "SEF_URL_TEMPLATES" => [
            "index" => 'index.php',
            "video" => 'video/#VIDEO_CODE#',
        ]
    )
); ?><?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>