<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Добавить альбом");
?>
<? $APPLICATION->IncludeComponent(
    "project:album.upload",
    "",
    [
        "IBLOCK_CODE" => "albums",
        "IBLOCK_TYPE" => "albums",
    ]
); ?>
<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>