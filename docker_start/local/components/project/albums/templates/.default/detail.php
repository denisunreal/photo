<? if (!CModule::IncludeModule("iblock")) {
    die();
}
?>
<?
global $APPLICATION;
$APPLICATION->IncludeComponent(
    "project:album.detail",
    ".default",
    array(
        "APARTMENT_ID" => "0",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "N",
        "CODE" => $arResult["VARIABLES"]["CODE"],
        "COMPONENT_TEMPLATE" => ".default",
        "IBLOCK_CODE" => "albums",
        "IBLOCK_TYPE" => "albums",
        "SHOW_NAV" => "Y",
        "COUNT" => "6",
        "SORT_FIELD1" => "ID",
        "SORT_DIRECTION1" => "ASC",
        "SORT_FIELD2" => "ID",
        "SORT_DIRECTION2" => "ASC",
        "HOUSING_CODE" => "0",
        "FLOOR_ID" => "0"
    ),
    false
); ?>
