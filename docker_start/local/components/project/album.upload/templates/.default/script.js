$(document).ready(function (){
    $(document).on('submit', '#img-upload', function (e){
        e.preventDefault(e);
        var form_data = new FormData();
        form_data.append("description", $('#ALBUM_DESCRIPTION').val());
        form_data.append("preview_photo", $('#img').prop('files')[0]);
        form_data.append("name", $('#ALBUM_NAME').val());
        form_data.append("AJAX_SUBMIT", "Y");
        console.log(form_data);
        $.ajax({
            url: $(form_data).attr('action'),
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            data: form_data,

            success:function (result){
                if(result["status"] === "success"){
                    window.location.replace("http://localhost/albums/");
                }
                else{
                    if (result["errors"]['name']!==undefined){
                        $('#nameER').show();
                    }
                    if (result["errors"]['desc']!==undefined){
                        $('#descER').show();
                    }
                }
            }
        });
    })
})
