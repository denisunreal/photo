<?php
$MESS['ALBUM_UPLOAD_IBLOCK_MODULE_NOT_INSTALLED'] = 'Модуль "Инфоблоки" не установлен';
$MESS['ALBUM_UPLOAD_PARAMETERS_SORT_ID'] = 'ID';
$MESS['ALBUM_UPLOAD_PARAMETERS_SORT_NAME'] = 'Название';
$MESS['ALBUM_UPLOAD_PARAMETERS_SORT_ACTIVE_FROM'] = 'Дата начала активности';
$MESS['ALBUM_UPLOAD_PARAMETERS_SORT_SORT'] = 'Индекс сортировки';
$MESS['ALBUM_UPLOAD_PARAMETERS_SORT_ASC'] = 'По возрастанию';
$MESS['ALBUM_UPLOAD_PARAMETERS_SORT_DESC'] = 'По убыванию';
$MESS['ALBUM_UPLOAD_PARAMETERS_IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['ALBUM_UPLOAD_PARAMETERS_IBLOCK_ID'] = 'Инфоблок';
$MESS['ALBUM_UPLOAD_PARAMETERS_SHOW_NAV'] = 'Постраничная навигация';
$MESS['ALBUM_UPLOAD_PARAMETERS_COUNT'] = 'Количество элементов';
$MESS['ALBUM_UPLOAD_PARAMETERS_SORT_FIELD1'] = 'Поле первой сортировки';
$MESS['ALBUM_UPLOAD_PARAMETERS_SORT_DIRECTION1'] = 'Направление первой сортировки';
$MESS['ALBUM_UPLOAD_PARAMETERS_SORT_FIELD2'] = 'Поле второй сортировки';
$MESS['ALBUM_UPLOAD_PARAMETERS_SORT_DIRECTION2'] = 'Направление второй сортировки';
$MESS['ALBUM_UPLOAD_PARAMETERS_IBLOCK_CODE'] = 'Код инфоблока';
$MESS['ALBUM_UPLOAD_PARAMETERS_HOUSING_CODE'] = 'Символьный код корпуса';

