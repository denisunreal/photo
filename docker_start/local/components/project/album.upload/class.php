<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

class AlbumUploadComponent extends StandardElementListComponent
{

    protected function executeEpilog()
    {
        if ($this->request->getPost('AJAX_SUBMIT') === 'Y') {
            $this->uploadAlbum();
        }
    }

    public function uploadAlbum()
    {
        if (CModule::IncludeModule("iblock")) {
            global $APPLICATION;
            global $USER;
            $APPLICATION->RestartBuffer();
            $result = [];
            $img = $_FILES['preview_photo'];
            if (empty($_POST['name'])) {
                $result['errors']['name'] = 'Введите наименование альбома';
            }
            if (empty($_POST['description'])) {
                $result['errors']['desc'] = 'Введите описание альбома';
            }
            if (!(empty($_POST['name'])) || !(empty($_POST['description']))
                || !(empty($_FILES['preview_photo']))
            ) {
                $el = new CIBlockElement;
                if (empty($img)) {
                    $arLoadProductArray = [
                        "MODIFIED_BY" => $USER->GetID(),
                        "IBLOCK_SECTION_ID" => false,
                        "IBLOCK_ID" => $this->arResult["IBLOCK_ID"],
                        "CODE" => $_POST['name'],
                        "NAME" => $_POST['name'],
                        "ACTIVE" => "Y",
                        "PREVIEW_TEXT" => $_POST['description'],
                    ];
                } else {
                    $VALUES["PHOTOS"][] = [
                        "VALUE" => $img,
                        "DESCRIPTION" => $_POST['description'],
                    ];
                    $arLoadProductArray = [
                        "MODIFIED_BY" => $USER->GetID(),
                        "IBLOCK_SECTION_ID" => false,
                        "IBLOCK_ID" => $this->arResult["IBLOCK_ID"],
                        "PROPERTY_VALUES" => $VALUES,
                        "CODE" => $_POST['name'],
                        "NAME" => $_POST['name'],
                        "ACTIVE" => "Y",
                        "PREVIEW_TEXT" => $_POST['description'],
                    ];
                }
            }
            if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
                $result["status"] = "success";
            } else {
                if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
                    $result["status"] = "error";
                } else {
                    $result["status"] = "error";
                }
            }
            header('Content-Type: application/json');
            echo(json_encode($result));
            die();
        }
    }

}
