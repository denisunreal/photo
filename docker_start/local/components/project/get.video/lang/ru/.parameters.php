<?php
$MESS['GET_VIDEO_IBLOCK_MODULE_NOT_INSTALLED'] = 'Модуль "Инфоблоки" не установлен';
$MESS['GET_VIDEO_PARAMETERS_SORT_ID'] = 'ID';
$MESS['GET_VIDEO_PARAMETERS_SORT_NAME'] = 'Название';
$MESS['GET_VIDEO_PARAMETERS_SORT_ACTIVE_FROM'] = 'Дата начала активности';
$MESS['GET_VIDEO_PARAMETERS_SORT_SORT'] = 'Индекс сортировки';
$MESS['GET_VIDEO_PARAMETERS_SORT_ASC'] = 'По возрастанию';
$MESS['GET_VIDEO_PARAMETERS_SORT_DESC'] = 'По убыванию';
$MESS['GET_VIDEO_PARAMETERS_IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['GET_VIDEO_PARAMETERS_IBLOCK_ID'] = 'Инфоблок';
$MESS['GET_VIDEO_PARAMETERS_SHOW_NAV'] = 'Постраничная навигация';
$MESS['GET_VIDEO_PARAMETERS_COUNT'] = 'Количество элементов';
$MESS['GET_VIDEO_PARAMETERS_SORT_FIELD1'] = 'Поле первой сортировки';
$MESS['GET_VIDEO_PARAMETERS_SORT_DIRECTION1'] = 'Направление первой сортировки';
$MESS['GET_VIDEO_PARAMETERS_IBLOCK_CODE'] = 'Код инфоблока';


