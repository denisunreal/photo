<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

use Bitrix\Main;

class GetVideoComponent extends StandardElementListComponent
{
    protected function checkParams()
    {
        if (empty($this->arParams['PLAYLIST_ID'])) {
            throw new Main\ArgumentNullException('PLAYLIST_ID');
        }
    }

    public function onPrepareComponentParams($params)
    {
        $result = [
            'PLAYLIST_ID' => (trim($params["PLAYLIST_ID"])),
            'SHOW_NAV' => ($params['SHOW_NAV'] == 'Y' ? 'Y' : 'N'),
            'COUNT' => intval($params['COUNT']),
            'CACHE_TIME' => intval($params['CACHE_TIME']) > 0
                ? intval($params['CACHE_TIME']) : 3600,
            'CACHE_TAG_OFF' => $params['CACHE_TAG_OFF'] == 'Y',
            'CACHE_TYPE' => $params['CACHE_TYPE'],
        ];
        return $result;
    }

    public function executeComponent()
    {
        try {
            $this->checkModules();
            $this->checkParams();
            $this->executeProlog();
            if (!$this->readDataFromCache()) {
                $this->getResult();
                $this->putDataToCache();
                $this->includeComponentTemplate($this->page);
            }
            return $this->returned;
        } catch (Exception $e) {
            $this->abortDataCache();
            ShowError($e->getMessage());
        }
    }

    public function getResult()
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://www.googleapis.com/youtube/v3/'
        ]);
        $playlistid = $this->arParams["PLAYLIST_ID"];
        $response = $client->request('GET',
            'playlistItems',
            [
                'query' => [
                    'part' => 'snippet',
                    'maxResults' => $this->arParams["COUNT"],
                    'playlistId' => $playlistid,
                    'key' => YOUTUBE_API_KEY
                ]
            ]);
        $video = json_decode($response->getBody(), true);
        $video_id = $video['items'];

        foreach ($video_id as $item) {
            $this->arResult["ITEMS"][] = [
                'TITLE' => $item["snippet"]['title'],
                'IMG_SRC' => $item["snippet"]['thumbnails']["high"]["url"],
                'URL' => $item["snippet"]["resourceId"]['videoId']
            ];
        }
    }
}
