<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

try {
    if (!Main\Loader::includeModule('iblock')) {
        throw new Main\LoaderException(Loc::getMessage('GET_VIDEO_IBLOCK_MODULE_NOT_INSTALLED'));
    }

    $iblockTypes = \CIBlockParameters::GetIBlockTypes(array('-' => ' '));

    $iblocks = array(0 => ' ');
    $iblocksCode = array('' => ' ');
    if (isset($arCurrentValues['IBLOCK_TYPE']) && strlen($arCurrentValues['IBLOCK_TYPE'])) {
        $filter = array(
            'TYPE' => $arCurrentValues['IBLOCK_TYPE'],
            'ACTIVE' => 'Y'
        );
        $iterator = \CIBlock::GetList(array('SORT' => 'ASC'), $filter);
        while ($iblock = $iterator->GetNext()) {
            $iblocks[$iblock['ID']] = $iblock['NAME'];
            $iblocksCode[$iblock['CODE']] = $iblock['NAME'];
        }
    }

    $sortFields = array(
        'ID' => Loc::getMessage('GET_VIDEO_PARAMETERS_SORT_ID'),
        'NAME' => Loc::getMessage('GET_VIDEO_PARAMETERS_SORT_NAME'),
        'ACTIVE_FROM' => Loc::getMessage('GET_VIDEO_PARAMETERS_SORT_ACTIVE_FROM'),
        'SORT' => Loc::getMessage('GET_VIDEO_PARAMETERS_SORT_SORT')
    );

    $sortDirection = array(
        'ASC' => Loc::getMessage('GET_VIDEO_PARAMETERS_SORT_ASC'),
        'DESC' => Loc::getMessage('GET_VIDEO_PARAMETERS_SORT_DESC')
    );

    $arComponentParameters = array(
        'GROUPS' => array(),
        'PARAMETERS' => array(
            'PLAYLIST_ID' => array(
                'PARENT' => 'BASE',
                'NAME' => 'ID плейлиста',
                'TYPE' => 'STRING'
            ),
            'SHOW_NAV' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('GET_VIDEO_PARAMETERS_SHOW_NAV'),
                'TYPE' => 'CHECKBOX',
                'DEFAULT' => 'N'
            ),
            'COUNT' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('GET_VIDEO_PARAMETERS_COUNT'),
                'TYPE' => 'STRING',
                'DEFAULT' => '0'
            ),
            'CACHE_TIME' => array(
                'DEFAULT' => 3600
            )
        )
    );
} catch (Main\LoaderException $e) {
    ShowError($e->getMessage());
}

