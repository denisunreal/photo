<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

\CBitrixComponent::includeComponentClass('system:standard.elements.list');

class AlbumDetailComponent extends StandardElementListComponent
{

    public function onPrepareComponentParams($params)
    {
        $params['AJAX_KEY'] = $params['AJAX_KEY'] ? $params['AJAX_KEY']
            : 'AJAX';
        $result = [
            'IBLOCK_TYPE' => trim($params['IBLOCK_TYPE']),
            'IBLOCK_ID' => intval($params['IBLOCK_ID']),
            'IBLOCK_CODE' => trim($params['IBLOCK_CODE']),
            "CODE" => trim($params["CODE"]),
            'SHOW_NAV' => ($params['SHOW_NAV'] == 'Y' ? 'Y' : 'N'),
            'COUNT' => intval($params['COUNT']),
            'SORT_FIELD1' => strlen($params['SORT_FIELD1'])
                ? $params['SORT_FIELD1'] : 'ID',
            'SORT_DIRECTION1' => $params['SORT_DIRECTION1'] == 'ASC' ? 'ASC'
                : 'DESC',
            'SORT_FIELD2' => strlen($params['SORT_FIELD2'])
                ? $params['SORT_FIELD2'] : 'ID',
            'SORT_DIRECTION2' => $params['SORT_DIRECTION2'] == 'ASC' ? 'ASC'
                : 'DESC',
            'CACHE_TIME' => intval($params['CACHE_TIME']) > 0
                ? intval($params['CACHE_TIME']) : 3600,
            'AJAX' => $params[$params['AJAX_KEY']] == 'N' ? 'N'
                : $_REQUEST[$params['AJAX_KEY']] == 'Y' ? 'Y' : 'N',
            'FILTER' => is_array($params['FILTER'])
            && sizeof($params['FILTER']) ? $params['FILTER'] : [],
            'CACHE_TAG_OFF' => $params['CACHE_TAG_OFF'] == 'Y',
            'CACHE_TYPE' => $params['CACHE_TYPE'],
            'ELEMENT_ID' => $params['ELEMENT_ID'],
        ];
        return $result;
    }

    public function getResult()
    {
        $arSelect = [
            "IBLOCK_ID",
            "ID",
            "NAME",
            "PREVIEW_TEXT",
            "CREATED_BY",
            "PROPERTY_PHOTOS",
        ];
        $arFilter = [
            "IBLOCK_ID" => $this->arResult["IBLOCK_ID"],
            "CODE" => $this->arParams["CODE"],
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y",
        ];
        $result = CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);
        $ob = $result->Fetch();
        foreach ($ob["PROPERTY_PHOTOS_VALUE"] as $key => $value) {
            $this->arResult["ITEMS"][] = [
                "IMG_SRC" => CFile::GetPath($value),
                "IMG_ID" => $ob["PROPERTY_PHOTOS_PROPERTY_VALUE_ID"][$key],
                "IMG_DSC" => $ob["PROPERTY_PHOTOS_DESCRIPTION"][$key],
            ];
        }
        $this->arResult["ALBUM_INFO"] = [
            "NAME" => $ob["NAME"],
            "ID" => $ob["ID"],
            "DESCRIPTION" => $ob["PREVIEW_TEXT"],
            "CREATOR" => $ob['CREATED_BY'],
        ];

    }

}
