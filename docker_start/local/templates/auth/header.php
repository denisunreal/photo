<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
IncludeTemplateLangFile(__FILE__);
?>
<!doctype html>
<html lang="en">
<head>
    <?

    use Bitrix\Main\Page\Asset;
    use Bitrix\Main\UI\Extension;

    Extension::load('ui.bootstrap4');
    $APPLICATION->ShowHead();
    ?>
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/template_style.css');
    Asset::getInstance()
        ->addString('<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">');
    Asset::getInstance()->addString('<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>');
    Asset::getInstance()->addString('<script src="https://www.google.com/recaptcha/api.js"></script>');

    /*if ($USER->IsAuthorized())
        LocalRedirect('/');
    */ ?>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/main.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/reg.js"></script>

</head>
<body class="text-center">



