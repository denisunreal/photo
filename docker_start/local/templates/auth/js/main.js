$(document).ready(function (){
    $(document).on('submit', '#form-signin', function (e){
        e.preventDefault();

        $.ajax({
            type: 'POST',
            cache: false,
            url: '/ajax/auth.php',
            data:$(this).serialize(),
            dataType:'json',
            success: function(result){
                if(result.status){
                    window.location.replace("http://localhost/");
                    /*location.reload();*/
                }
                else{
                    $('.message.error').html(result.message);
                    $('.message.error').show();
                }
            }
        })
    })
})