<?php

$filter = [
    'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
    'ACTIVE' => 'Y',
];
$sort = [
    $arParams['SORT_FIELD1'] => $arParams['SORT_DIRECTION1']
];
$select = [
    'ID',
    'NAME',
    'CODE',
    "CREATED_BY",
    'DATE_ACTIVE_FROM',
    'DETAIL_TEXT',
    'DETAIL_PAGE_URL',
    'CREATED_USER_NAME',
    'PREVIEW_TEXT',
    'PREVIEW_TEXT_TYPE',
    'PREVIEW_PICTURE',
    'PROPERTY_PHOTO'
];

$iterator = \CIBlockElement::GetList($sort, $filter, false, $navParams, $select);

$i = 0;
while ($element = $iterator->GetNext()) {
    $res = CIBlockElement::GetProperty($arResult["IBLOCK_ID"], $element["ID"], "sort", "asc", array());
    $ob = $res->Fetch();
    $img_src = CFile::GetPath($ob["VALUE"]);
    if ($img_src == null) {
        $img_src = SITE_TEMPLATE_PATH . "/placeholder.png";
    }
    $arResult['ITEMS'][$i] += [
        'IMG_SRC' => $img_src
    ];
    $i++;
}
