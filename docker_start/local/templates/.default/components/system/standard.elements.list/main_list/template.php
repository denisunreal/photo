<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?php

use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);
$this->setFrameMode(true);
?>
<main role="main">
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <?php if (count($arResult['ITEMS'])): ?>
                    <?php foreach ($arResult['ITEMS'] as $item): ?>
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <a href="<?= $item["URL"] ?>">
                                    <img class="card-img-top" src='<?= $item["IMG_SRC"] ?>' alt="<? $item["NAME"] ?>"
                                         height="225">
                                </a>
                                <div class="card-body">
                                    <p class="card-text"><? echo $item["NAME"]; ?></p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <button onclick="document.location='<?= $item["URL"] ?>'"
                                                    type="button" class="btn btn-sm btn-outline-secondary">Просмотр
                                            </button>
                                        </div>
                                        <small class="text-muted"><? echo $item["CREATED_USER_NAME"]; ?></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <? else: ?>
            </div>
        </div>
    </div>
</main>
<?php endif; ?>
