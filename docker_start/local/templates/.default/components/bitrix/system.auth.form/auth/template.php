<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

CJSCore::Init();
?>
<div id="ajax-auth-form-wrapper">
    <div class="bx-system-auth-form">


        <? if ($arResult["FORM_TYPE"] == "login"): ?>
            <form class="form-signin" name="login-form" method="post" target="_top" id="form-signin"
                  action="<?= $arResult["AUTH_URL"] ?>">
                <input type="hidden" name="AJAX-ACTION" value="AUTH"/>
                <img class="mb-4"
                     src="<? $_SERVER["DOCUMENT_ROOT"] ?>/local/templates/.default/components/bitrix/system.auth.form/auth/images/camera.png"
                     alt="" width="72" height="72">
                <h1 class="h3 mb-3 font-weight-normal">Пожалуйста авторизируйтесь</h1>
                <? if ($arResult["BACKURL"] <> ''): ?>
                    <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
                <? endif ?>
                <? foreach ($arResult["POST"] as $key => $value): ?>
                    <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
                <? endforeach ?>
                <input type="hidden" name="AUTH_FORM" value="Y"/>
                <input type="hidden" name="TYPE" value="AUTH"/>
                <label class="sr-only"><?= GetMessage("AUTH_LOGIN") ?>:</label>
                <input type="text" name="USER_LOGIN" class="form-control" placeholder="Email address" required
                       autofocus/>
                <label class="message error"></label>
                <script>
                    BX.ready(function () {
                        var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
                        if (loginCookie) {
                            var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
                            var loginInput = form.elements["USER_LOGIN"];
                            loginInput.value = loginCookie;
                        }
                    });
                </script>
                <label for="inputPassword" class="sr-only"><?= GetMessage("AUTH_PASSWORD") ?>:</label>
                <input name="USER_PASSWORD" type="password" id="inputPassword" class="form-control"
                       placeholder="Password"
                       required>
                <? if ($arResult["STORE_PASSWORD"] == "Y"): ?>
                    <div class="checkbox mb-3">
                        <label>
                            <input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y"/>
                            <label for="USER_REMEMBER_frm"
                                   title="<?= GetMessage("AUTH_REMEMBER_ME") ?>"><? echo GetMessage("AUTH_REMEMBER_SHORT") ?></label>
                        </label>
                    </div>
                <? endif ?>
                <button class="btn btn-lg btn-primary btn-block" type="submit"
                        name="login"><?= GetMessage("AUTH_LOGIN_BUTTON") ?>
                </button>
                <? if ($arResult["NEW_USER_REGISTRATION"] == "Y"): ?>
                    <tr>
                        <td colspan="2">
                            <noindex><a href="<?= $arResult["AUTH_REGISTER_URL"] ?>"
                                        rel="nofollow"><?= GetMessage("AUTH_REGISTER") ?></a></noindex>
                            <br/></td>
                    </tr>
                <? endif ?>
                <?
                if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']) {
                    ShowMessage($arResult['ERROR_MESSAGE']);
                }
                ?>

            </form>

        <? else: ?>
            <form action="<?= $arResult["AUTH_URL"] ?>">
                <table width="95%">
                    <tr>
                        <td align="center">
                            <?= $arResult["USER_NAME"] ?><br/>
                            [<?= $arResult["USER_LOGIN"] ?>]<br/>
                            <a href="<?= $arResult["PROFILE_URL"] ?>"
                               title="<?= GetMessage("AUTH_PROFILE") ?>"><?= GetMessage("AUTH_PROFILE") ?></a><br/>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <? foreach ($arResult["GET"] as $key => $value): ?>
                                <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
                            <? endforeach ?>
                            <input type="hidden" name="logout" value="yes"/>
                            <input type="submit" name="logout_butt" value="<?= GetMessage("AUTH_LOGOUT_BUTTON") ?>"/>
                        </td>
                    </tr>
                </table>
            </form>
        <? endif ?>
    </div>
</div>
