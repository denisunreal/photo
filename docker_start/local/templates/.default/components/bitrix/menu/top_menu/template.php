<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
IncludeTemplateLangFile(__FILE__); ?>

<? if (!empty($arResult)): ?>
    <h4 class="text-white">Главное меню</h4>
    <ul class="list-unstyled">

        <?
        foreach ($arResult as $arItem):
            if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) {
                continue;
            }
            ?>
            <? if ($arItem["SELECTED"]):?>
            <li><a href="<?= $arItem["LINK"] ?>" class="text-white"><?= $arItem["TEXT"] ?></a></li>
        <? else:?>
            <li><a href="<?= $arItem["LINK"] ?> " class="text-white"><?= $arItem["TEXT"] ?></a></li>
        <? endif ?>
        <? endforeach ?>
    </ul>
<? endif ?>
