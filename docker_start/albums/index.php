<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
global $APPLICATION;
$APPLICATION->SetTitle("Альбомы");
?><? $APPLICATION->IncludeComponent(
  "project:albums",
  "",
  [
    "SEF_FOLDER"        => "/albums/",
    "SEF_MODE"          => "Y",
    "SEF_URL_TEMPLATES" =>
      [
        "index"  => 'index.php',
        "detail" => '#CODE#/',
        "upload" => '#CODE#/upload',
        "add"    => 'add',
        "logout" => 'logout',
        "auth"   => 'auth.php',
      ],
  ]
); ?><?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>