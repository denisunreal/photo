<?php
require($_SERVER["DOCUMENT_ROOT"]
  . "/bitrix/modules/main/include/prolog_before.php");
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
if (CModule::IncludeModule("iblock")) {
    $result = [];
    global $USER, $DB;
    $iblock_id = $_POST["iblock_id"];
    $album_id = $_POST['data'];
    $arSelect = ["ID", "IBLOCK_ID", "CREATED_BY"];
    $arFilter = ["IBLOCK_ID"   => $iblock_id,
                 "ID"          => $album_id,
                 "ACTIVE_DATE" => "Y",
                 "ACTIVE"      => "Y",
    ];
    $res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50],
      $arSelect);
    $ob = $res->GetNextElement();
    $arFields = $ob->GetFields();
    $creator_id = $arFields["CREATED_BY"];
    if ($USER->GetID() == $creator_id) {
        if (!CIBlockElement::Delete($album_id)) {
            $result["status"] = "error";
        } else {
            $result["status"] = "success";
        }
    } else {
        $result["status"] = "error";
    }
    header('Content-Type: application/json');
    echo(json_encode($result));
}