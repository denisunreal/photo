<?php
require($_SERVER["DOCUMENT_ROOT"]
  . "/bitrix/modules/main/include/prolog_before.php");
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
if (CModule::IncludeModule("iblock")) {
    global $USER;
    $result = [];
    $photo_id = $_POST["photo_id"];
    $album_id = $_POST["album_id"];
    $iblock_id = $_POST["iblock_id"];
    $album_code = $_POST["album_code"];
    $creator_id = $_POST["creator_id"];
    $arSelect = ["ID", "IBLOCK_ID", "CREATED_BY"];
    $arFilter = ["IBLOCK_ID"   => $iblock_id,
                 "CODE"        => $album_code,
                 "ACTIVE_DATE" => "Y",
                 "ACTIVE"      => "Y",
    ];
    $arFile["del"] = "Y";
    $res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 6],
      $arSelect);
    $ob = $res->GetNextElement();
    $arFields = $ob->GetFields();
    $creator_id = $arFields["CREATED_BY"];
    if ($USER->GetID() == $creator_id) {
        if (!(CIBlockElement::SetPropertyValueCode($album_id, "PHOTOS",
          [$photo_id => ["VALUE" => $arFile]]))
        ) {
            $result["status"] = "error";
        } else {
            CIBlock::clearIBlockTagCache($iblock_id);
            $result["status"] = "success";
        }
    } else {
        $result["status"] = "error";
    }
    header('Content-Type: application/json');
    echo(json_encode($result));
}