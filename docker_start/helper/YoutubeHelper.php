<?php
namespace YoutubeHelper;
class YoutubeHelper
{
    public function getVideoByID($videoId)
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://youtube.googleapis.com/youtube/v3/'
        ]);
        $response = $client->request('GET',
            'videos',
            [
                'query' => [
                    'part' => 'snippet',
                    'id' => $videoId,
                    'key' => YOUTUBE_API_KEY
                ]
            ]);
        $video_info = json_decode($response->getBody(),true);
        return $video_info;
    }
}