<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
global $USER;
$USER->Logout();
LocalRedirect("/");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");